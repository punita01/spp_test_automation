import json
import logging
from optparse import OptionParser
import copy
import sys
import client
import datetime
import time
import os
import Tkinter
import tkFileDialog
from os.path import isfile,join
logging.basicConfig()
logger = logging.getLogger('logger')
logger.setLevel(logging.INFO) 

def oracle_backup():
    
    all_databases = client.SppAPI(session, 'api/application/oracle').get(path="/instance?from=hlo")
    instances = all_databases['instances']
    #print instances
    backup_instance = userinput['oracle_backup_instance']
    for inst in instances:
        if(inst['name'] == backup_instance):
                backup_href = inst['links']['self']['href']
                backup_id = inst['id']
                backup_metadataPath = inst['metadataPath']
                
    oracle_options = userinput['oracle_options']
    if(oracle_options == '1'):
        max_streams = userinput['oracle_max_streams']
        applyOptions = {"resources":[{"href": backup_href,"id":backup_id,"metadataPath":backup_metadataPath}],"options":{"maxParallelStreams":max_streams}}
        backup_opt = client.SppAPI(session, 'ngp/application').post(path='?action=applyOptions', data=applyOptions)
        print ("Options updated for the Oracle instance!")
    elif(oracle_options == '2'):
        all_policies_data = client.SppAPI(session, 'ngp/slapolicy').get(path="?status=true&subtype=oracle")
        all_policies = all_policies_data['slapolicies']
        assign_sla = userinput['oracle_assign_sla']
        instance_list = backup_instance.split(",")
        instance_info = []
        instdata = {}
        for inst in instance_list:
            for each_inst in instances:
                if(each_inst['name'] == inst):
                    instdata['href'] = each_inst['links']['self']['href']
                    instdata['id'] = each_inst['id']
                    instdata['metadataPath'] = each_inst['metadataPath']
                    instance_info.append(copy.deepcopy(instdata))
                    logger.info("Adding instance " + inst + " to SLA ")
                    break

        slaarray = []
        sla_list = assign_sla.split(",")
        sladata = {}
        for sla in sla_list:
            for each_sla in all_policies:
                if(each_sla['name'] == sla):
                    sladata['href'] = each_sla['links']['self']['href']
                    sladata['id'] = each_sla['id']
                    sladata['name'] = each_sla['name']
                    slaarray.append(copy.deepcopy(sladata))
                    break
            if not slaarray:
                logger.error("No SLA Policy found with name " + user_sla)
                session.logout()
                sys.exit(2)
        
        assigndata = {}
        assigndata['subtype'] = "oracle"
        assigndata['version'] = "1.0"
        assigndata['resources'] = instance_info
        assigndata['slapolicies'] = slaarray
        resp = client.SppAPI(session, 'ngp/application').post(path='?action=applySLAPolicies', data=assigndata)
        logger.info("Oracle instances are now assigned")
        
        